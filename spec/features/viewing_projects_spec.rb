require "rails_helper"

RSpec.feature "Users can view projects" do
  scenario "with the project details" do
    project = FactoryGirl.create(:project, name: "Emacs")

    visit "/"
    click_link "Emacs"
    expect(page.current_url).to eq project_url(project)
  end
end
